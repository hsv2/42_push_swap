/* ************************************************************************** */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*   By: ablanken <ablanken at student dot 42barcelona dot com                */
/*                                                                            */
/*   Created: foo bar by ablanken                                             */
/*   Updated: foo bar by Andrea Blanke                                        */
/*                                                                            */
/* ************************************************************************** */
#include <push_swap.h>

int main(int c, char **v)
{
	t_stack *stack_a;

	if (c < 2)
		return (-1);
	stack_a = push_swap_stack_from_args(&v[1], c - 1);
	if (!stack_a)
		return (-2);
	t_push_swap_ctx *ctx = push_swap_ctx_new(stack_a);
	if (!ctx)
		return (-3);
	push_swap_sort(ctx);
	push_swap_ctx_del(ctx);
	stack_del(stack_a);
	return (0);
}
// vi: noet sw=4 ts=4:
