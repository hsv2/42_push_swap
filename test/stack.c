#include <stdio.h>

#include <ft_stack.h>

#include "../src/stack/stack_data.h"

//#define printf(...) (void)0
//#define putchar(...) (void)0
#define STACKSIZE 10

static void print_stack(t_stack *s);

int main(void)
{
    t_stack *s = stack_new(STACKSIZE, sizeof (long));

    for (long item = -(STACKSIZE / 2); item < STACKSIZE / 2; item++)
    {
        printf("push %ld\n", item);
        stack_push(s, &item);
        print_stack(s);
    }

    printf("pop %ld\n", *(long int*)stack_pop(s).item);
    print_stack(s);

    printf("right rotate step 1\n");
    stack_rrotate(s, 1);
    print_stack(s);
    printf("right rotate step 3\n");
    stack_rrotate(s, 3);
    print_stack(s);
    printf("left rotate step 3\n");
    stack_lrotate(s, 3);
    print_stack(s);
    printf("swap\n");
    stack_swap(s);
    print_stack(s);

    stack_del(s);

    return 0;
}

static void print_stack(t_stack *s)
{
    const t_stack_view	view = stack_view(s);

    for (size_t i = 0; i < view.top; i++)
        printf("%ld  ", *(long*)arr_get(view.raw, i).item);
    putchar('\n');
}
