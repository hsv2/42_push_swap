/* ************************************************************************** */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*   By: ablanken <ablanken at student dot 42barcelona dot com                */
/*                                                                            */
/*   Created: foo bar by ablanken                                             */
/*   Updated: foo bar by Andrea Blanke                                        */
/*                                                                            */
/* ************************************************************************** */

#include <ft_stack.h>

#include <limits.h>

int	stack_int_maxmin(const t_stack *s, int *max, int *min)
{
	int				value;
	t_stack_view	view;

	if (stack_itemsize(s) != sizeof (int) || !stack_height(s))
		return (-1);
	*max = INT_MIN;
	*min = INT_MAX;
	view = stack_view(s);
	while (view.top--)
	{
		value = arr_int_get(view.raw, view.top);
		if (value > *max)
			*max = value;
		if (value < *min)
			*min = value;
	}
	return (0);
}

int	stack_int_issorted(const t_stack *s)
{
	int				prev;
	t_stack_view	view;

	if (stack_itemsize(s) != sizeof (int))
		return (0);
	view = stack_view(s);
	prev = INT_MIN;
	while (view.top--)
	{
		if (prev > arr_int_get(view.raw, view.top))
			return (0);
		prev = arr_int_get(view.raw, view.top);
	}
	return (1);
}

int	stack_int_uniq(const t_stack *s)
{
	return (arr_int_uniq(stack_view(s).raw, 0, stack_height(s)));
}

// vi: noet sw=4 ts=4:
