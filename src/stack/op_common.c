/* ************************************************************************** */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*   By: ablanken <ablanken at student dot 42barcelona dot com                */
/*                                                                            */
/*   Created: foo bar by ablanken                                             */
/*   Updated: foo bar by Andrea Blanke                                        */
/*                                                                            */
/* ************************************************************************** */
#include <ft_stack.h>

#include <libft.h>

#include "stack_data.h"

t_array_item	stack_peek(const t_stack *s)
{
	if (s->top < 1 || s->top > stack_maxheight(s))
		return (ARRAY_NOITEM);
	return (arr_get(s->impl, s->top - 1));
}

t_array_item	stack_pop(t_stack *s)
{
	if (s->top < 1 || s->top > stack_maxheight(s))
		return (ARRAY_NOITEM);
	return (arr_get(s->impl, --(s->top)));
}

t_array_item	stack_push(t_stack *s, void *item)
{
	if (s->top >= stack_maxheight(s) || !item)
		return (ARRAY_NOITEM);
	return (arr_set(s->impl, s->top++, item));
}

// vi: noet sw=4 ts=4:
