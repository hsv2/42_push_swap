/* ************************************************************************** */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*   By: ablanken <ablanken at student dot 42barcelona dot com                */
/*                                                                            */
/*   Created: foo bar by ablanken                                             */
/*   Updated: foo bar by Andrea Blanke                                        */
/*                                                                            */
/* ************************************************************************** */

#include <ft_stack.h>

#include <stdlib.h>

#include "stack_data.h"

t_stack	*stack_new(size_t count, size_t size)
{
	t_stack	*s;

	s = malloc(sizeof (t_stack));
	if (s)
	{
		s->impl = arr_new(size, count);
		if (s->impl)
			s->top = 0;
		else
			s = stack_del(s);
	}
	return (s);
}

void	*stack_del(t_stack *s)
{
	arr_del(s->impl);
	free(s);
	return (NULL);
}

// vi: noet sw=4 ts=4:
