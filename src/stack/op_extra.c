/* ************************************************************************** */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*   By: ablanken <ablanken at student dot 42barcelona dot com                */
/*                                                                            */
/*   Created: foo bar by ablanken                                             */
/*   Updated: foo bar by Andrea Blanke                                        */
/*                                                                            */
/* ************************************************************************** */

#include <ft_stack.h>

#include <stdlib.h>

#include "stack_data.h"

t_stack	*stack_rrotate(t_stack *s, unsigned int step)
{
	arr_rev(s->impl, 0, s->top - 1);
	arr_rev(s->impl, 0, step - 1);
	arr_rev(s->impl, step, s->top - 1);
	return (s);
}

t_stack	*stack_lrotate(t_stack *s, unsigned int step)
{
	if (arr_rev(s->impl, 0, step - 1) && arr_rev(s->impl, step, s->top - 1)
			&& arr_rev(s->impl, 0, s->top - 1))
		return (s);
	return (NULL);
}

t_stack	*stack_swap(t_stack *s)
{
	if (!arr_swap(s->impl, s->top - 1, s->top - 2))
		return (NULL);
	return (s);
}

// vi: noet sw=4 ts=4:
