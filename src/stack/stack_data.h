/* ************************************************************************** */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*   By: ablanken <ablanken at student dot 42barcelona dot com                */
/*                                                                            */
/*   Created: foo bar by ablanken                                             */
/*   Updated: foo bar by Andrea Blanke                                        */
/*                                                                            */
/* ************************************************************************** */
#ifndef  STACK_DATA_H
# define STACK_DATA_H

# include <stddef.h>

typedef struct s_array	t_array;

typedef struct s_stack
{
	size_t	top;
	t_array	*impl;
}			t_stack;

#endif
// vi: noet sw=4 ts=4:
