/* ************************************************************************** */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*   By: ablanken <ablanken at student dot 42barcelona dot com                */
/*                                                                            */
/*   Created: foo bar by ablanken                                             */
/*   Updated: foo bar by Andrea Blanke                                        */
/*                                                                            */
/* ************************************************************************** */

#include <ft_stack.h>

#include "stack_data.h"

size_t	stack_height(const t_stack *s)
{
	return (s->top);
}

size_t	stack_itemsize(const t_stack *s)
{
	return (arr_itemsize(s->impl));
}

size_t	stack_maxheight(const t_stack *s)
{
	return (arr_size(s->impl));
}

t_stack_view	stack_view(const t_stack *s)
{
	return ((t_stack_view){stack_maxheight(s), s->top, s->impl});
}

// vi: noet sw=4 ts=4:
