/* ************************************************************************** */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*   By: ablanken <ablanken at student dot 42barcelona dot com                */
/*                                                                            */
/*   Created: foo bar by ablanken                                             */
/*   Updated: foo bar by Andrea Blanke                                        */
/*                                                                            */
/* ************************************************************************** */

#include <ft_array.h>

#include <limits.h>
#include <stdlib.h>

#include <libft.h>

#include "ft_array_data.h"

t_array	*arr_rev(t_array *a, size_t start, size_t end)
{
	while (start < end)
	{
		if (!arr_swap(a, start++, end--))
			return (NULL);
	}
	return (a);
}

t_array	*arr_swap(t_array *a, size_t idxa, size_t idxb)
{
	unsigned char	tmp;
	unsigned char	*item_a;
	unsigned char	*item_b;
	size_t			offset;

	item_a = arr_get(a, idxa).item;
	item_b = arr_get(a, idxb).item;
	if (!item_a || !item_b)
		return (NULL);
	offset = 0;
	while (offset < a->itemsize)
	{
		tmp = item_a[offset];
		item_a[offset] = item_b[offset];
		item_b[offset] = tmp;
		offset++;
	}
	return (a);
}

int	arr_int_get(const t_array *a, size_t idx)
{
	if (a->itemsize != sizeof (int) || idx >= a->itemcount)
		return (INT_MIN);
	return (*(int *)arr_get(a, idx).item);
}

int	arr_int_uniq(const t_array *a, size_t start, size_t end)
{
	size_t	i;
	size_t	tmp;

	if (a->itemsize != sizeof (int))
		return (INT_MIN);
	if (start > end)
	{
		tmp = start;
		start = end;
		end = tmp;
	}
	while (start < end - 1)
	{
		i = start + 1;
		while (i < end)
		{
			if (arr_int_get(a, start) == arr_int_get(a, i))
				return (0);
			i++;
		}
		start++;
	}
	return (1);
}

// vi: noet sw=4 ts=4:
