/* ************************************************************************** */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*   By: ablanken <ablanken at student dot 42barcelona dot com                */
/*                                                                            */
/*   Created: foo bar by ablanken                                             */
/*   Updated: foo bar by Andrea Blanke                                        */
/*                                                                            */
/* ************************************************************************** */

#include <ft_array.h>

#include <libft.h>

#include "ft_array_data.h"

t_array_item	arr_get(const t_array *a, size_t idx)
{
	if (idx < a->itemcount)
		return ((t_array_item){a->itemsize, &a->bytes[idx * a->itemsize]});
	return (ARRAY_NOITEM);
}

t_array_item	arr_set(t_array *a, size_t idx, void *item)
{
	if (idx < a->itemcount)
		return ((t_array_item) {
				a->itemsize,
				ft_memmove(&a->bytes[idx * a->itemsize], item, a->itemsize),
			});
	return (ARRAY_NOITEM);
}

size_t	arr_size(const t_array *a)
{
	return (a->itemcount);
}

size_t	arr_itemsize(const t_array *a)
{
	return (a->itemsize);
}

// vi: noet sw=4 ts=4:
