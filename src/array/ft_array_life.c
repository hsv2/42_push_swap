/* ************************************************************************** */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*   By: ablanken <ablanken at student dot 42barcelona dot com                */
/*                                                                            */
/*   Created: foo bar by ablanken                                             */
/*   Updated: foo bar by Andrea Blanke                                        */
/*                                                                            */
/* ************************************************************************** */

#include <ft_array.h>

#include <stdlib.h>

#include "ft_array_data.h"

t_array	*arr_new(size_t isize, size_t icount)
{
	t_array	*const a = malloc(sizeof (t_array));

	if (!a)
		return (NULL);
	a->bytes = malloc(isize * icount);
	if (!a->bytes)
		return (arr_del(a));
	a->itemsize = isize;
	a->itemcount = icount;
	return (a);
}

void	*arr_del(t_array *a)
{
	free(a->bytes);
	free(a);
	return (NULL);
}

// vi: noet sw=4 ts=4:
