/* ************************************************************************** */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*   By: ablanken <ablanken at student dot 42barcelona dot com                */
/*                                                                            */
/*   Created: foo bar by ablanken                                             */
/*   Updated: foo bar by Andrea Blanke                                        */
/*                                                                            */
/* ************************************************************************** */

#ifndef  FT_ARRAY_DATA_H
# define FT_ARRAY_DATA_H

# include <stddef.h>

struct s_array
{
	size_t			itemcount;
	size_t			itemsize;
	unsigned char	*bytes;
};

#endif
// vi: noet sw=4 ts=4:
