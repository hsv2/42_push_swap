/* ************************************************************************** */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*   By: ablanken <ablanken at student dot 42barcelona dot com                */
/*                                                                            */
/*   Created: foo bar by ablanken                                             */
/*   Updated: foo bar by Andrea Blanke                                        */
/*                                                                            */
/* ************************************************************************** */
#include "push_swap_info.h"

#include <stdlib.h>

#include <ft_stack.h>

t_stack_info	*stack_info_new(const t_stack *s)
{
	t_stack_info	*const info = malloc(sizeof (t_stack_info));
	t_stack_view	sview;

	if (!info)
		return (NULL);
	info->items = NULL;
	if (stack_int_maxmin(s, &info->max, &info->min) < 0)
		return (stack_info_del(info));
	info->items = malloc(sizeof (t_stackitem_info) * stack_height(s));
	if (!info->items)
		return (stack_info_del(info));
	info->stack = s;
	sview = stack_view(s);
	while (sview.top-- > 0)
		info->items[sview.top].val = arr_int_get(sview.raw, sview.top);
	stack_info_update(info, 1);
	return (info);
}

void	*stack_info_del(t_stack_info *i)
{
	free(i->items);
	free(i);
	return (NULL);
}
// vi: noet sw=4 ts=4:
