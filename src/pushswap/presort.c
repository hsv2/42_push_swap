/* ************************************************************************** */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*   By: ablanken <ablanken at student dot 42barcelona dot com                */
/*                                                                            */
/*   Created: foo bar by ablanken                                             */
/*   Updated: foo bar by Andrea Blanke                                        */
/*                                                                            */
/* ************************************************************************** */
#include "ft_array.h"
#include "push_swap.h"
#include "push_swap_info.h"
#include "sort.h"

#include <limits.h>
#include <stdio.h>

#include "push_swap_ctx.h"

void	range_presort(t_push_swap_ctx *ctx, size_t target)
{
	t_stackitem_info	*item;
	int					max;
	t_stack_view		sviewa;
	t_stack_view		sviewb;
	size_t				i;
	size_t				range;

	if (target < 4)
	{
		target = 1;
		range = 4;
	}
	else
		range = target;

	printf("presort starting rank %lu\n", target);
	while (target)
	{
		stack_info_update(ctx->sinfo, 0);
		item = stack_info_getitem_bytarget(ctx->sinfo, target);
		if (!item)
		{
			max = INT_MAX;
			target = 0;
		}
		else
		{
			max = item->val;
			target += range;
		}
		sviewa = stack_view(ctx->stack_a);
		sviewb = stack_view(ctx->stack_b);
		i = 0;
		printf("Searching numbers lesser than %d\n", max);
		while (i < sviewa.top)
		{
			stack_info_update(ctx->sinfo, 1);
			item = stack_info_getitem_bypos(ctx->sinfo, i);
			if (item->val <= max)
			{
				printf("Found %d at pos %lu, target %lu\n", item->val, item->pos, item->target_pos);
				move_item(ctx, ctx->stack_a, item->pos, 0);
				// if (sviewa.top > 1 && sviewb.top > 1
				// 		&& arr_int_get(sviewa.raw, sviewa.top - 1)> arr_int_get(sviewa.raw, sviewa.top - 2)
				// 		&& arr_int_get(sviewb.raw, sviewb.top - 1) < arr_int_get(sviewb.raw, sviewb.top - 2))
				// 	push_swap_exec(ctx, ps_op_ss);
				// else if (sviewa.top > 1
				// 		&& arr_int_get(sviewa.raw, sviewa.top - 1) > arr_int_get(sviewa.raw, sviewa.top - 2))
				// 	push_swap_exec(ctx, ps_op_sa);
				// else if (sviewb.top > 1
				// 		&& arr_int_get(sviewb.raw, sviewb.top - 1) < arr_int_get(sviewb.raw, sviewb.top - 2))
				// 	push_swap_exec(ctx, ps_op_sb);
				push_swap_exec(ctx, ps_op_pb);
				sviewa.top--;
				sviewb.top++;
				i = 0;
				continue;
			}
			i++;
		}
	}
}
// vi: noet sw=4 ts=4:
