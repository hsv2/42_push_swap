/* ************************************************************************** */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*   By: ablanken <ablanken at student dot 42barcelona dot com                */
/*                                                                            */
/*   Created: foo bar by ablanken                                             */
/*   Updated: foo bar by Andrea Blanke                                        */
/*                                                                            */
/* ************************************************************************** */
#include <push_swap.h>

#include <libft.h>

const char	*push_swap_name_from_op(t_push_swap_op op)
{
	if (op == ps_op_pa)
		return "push a";
	else if (op == ps_op_pb)
		return "push b";
	else if (op == ps_op_ra)
		return "rotate a";
	else if (op == ps_op_rb)
		return "rotate b";
	else if (op == ps_op_rr)
		return "rotate both";
	else if (op == ps_op_rra)
		return "reverse rotate a";
	else if (op == ps_op_rrb)
		return "reverse rotate b";
	else if (op == ps_op_rrr)
		return "reverse rotate both";
	else if (op == ps_op_sa)
		return "swap a";
	else if (op == ps_op_sb)
		return "swap b";
	else if (op == ps_op_ss)
		return "swap both";
	return ("Unknown");
}

t_push_swap_op	push_swap_op_from_name(const char *opname)
{
	if (ft_strncmp(opname, "pa", 4))
		return (ps_op_pa);
	else if (ft_strncmp(opname, "pb", 4))
		return (ps_op_pb);
	else if (ft_strncmp(opname, "ra", 4))
		return (ps_op_ra);
	else if (ft_strncmp(opname, "rb", 4))
		return (ps_op_rb);
	else if (ft_strncmp(opname, "rr", 4))
		return (ps_op_rr);
	else if (ft_strncmp(opname, "rra", 4))
		return (ps_op_rra);
	else if (ft_strncmp(opname, "rrb", 4))
		return (ps_op_rrb);
	else if (ft_strncmp(opname, "rrr", 4))
		return (ps_op_rrr);
	else if (ft_strncmp(opname, "sa", 4))
		return (ps_op_sa);
	else if (ft_strncmp(opname, "sb", 4))
		return (ps_op_sb);
	else if (ft_strncmp(opname, "ss", 4))
		return (ps_op_ss);
	return (ps_op_undefined);
}
// vi: noet sw=4 ts=4:
