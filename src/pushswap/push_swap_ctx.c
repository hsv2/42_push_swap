/* ************************************************************************** */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*   By: ablanken <ablanken at student dot 42barcelona dot com                */
/*                                                                            */
/*   Created: foo bar by ablanken                                             */
/*   Updated: foo bar by Andrea Blanke                                        */
/*                                                                            */
/* ************************************************************************** */
#include "push_swap_ctx.h"

#include <stdlib.h>

#include <libft.h>

static inline int	default_step_cb(t_stack_view _, t_stack_view __,
		t_push_swap_op ___, void *____)
{
	(void)_;
	(void)__;
	(void)___;
	(void)____;
	return (1);
}

t_push_swap_ctx	*push_swap_ctx_new(t_stack *stack_a)
{
	t_push_swap_ctx	*ctx;

	if (!stack_int_uniq(stack_a))
		return (NULL);
	ctx = ft_calloc(1, sizeof (t_push_swap_ctx));
	if (!ctx)
		return (NULL);
	ctx->stack_b = stack_new(stack_height(stack_a), stack_itemsize(stack_a));
	if (!ctx->stack_b)
		return (push_swap_ctx_del(ctx));
	ctx->stack_a = stack_a;
	ctx->cb = default_step_cb;
	ctx->cb_payload = NULL;
	ctx->sinfo = NULL;
	return (ctx);
}

void	*push_swap_ctx_del(t_push_swap_ctx *ctx)
{
	stack_del(ctx->stack_b);
	free(ctx);
	return (NULL);
}

t_stack	*push_swap_stack_from_args(char **args, int count)
{
	t_stack	*s;
	int		nbr;

	s = stack_new(count, sizeof (int));
	while (s && count-- > 0)
	{
		if (ft_atoi_strict(args[count], &nbr) == 0)
			stack_push(s, &nbr);
		else
			return (stack_del(s));
	}
	return (s);
}

void	push_swap_step_cb(t_push_swap_ctx *ctx,
		int (*cb)(t_stack_view, t_stack_view, t_push_swap_op, void *),
		void *payload)
{
	if (!cb)
	{
		ctx->cb = default_step_cb;
		return ;
	}
	ctx->cb = cb;
	ctx->cb_payload = payload;
}
// vi: noet sw=4 ts=4:
