/* ************************************************************************** */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*   By: ablanken <ablanken at student dot 42barcelona dot com                */
/*                                                                            */
/*   Created: foo bar by ablanken                                             */
/*   Updated: foo bar by Andrea Blanke                                        */
/*                                                                            */
/* ************************************************************************** */
#include "push_swap_info.h"

#include <limits.h>

#include <ft_stack.h>

static int	index_items(t_stack_info *sinfo);

t_stack_info	*stack_info_update(t_stack_info *sinfo, int reindex)
{
	size_t				pos;
	t_stack_view		sview;

	if (reindex)
		if (index_items(sinfo) < 0)
			return (NULL);
	pos = 0;
	sview = stack_view(sinfo->stack);
	while (sview.top-- > 0)
	{
		stack_info_getitem_byvalue(sinfo,
				arr_int_get(sview.raw, sview.top))->pos = pos;
		pos++;
	}
	return (sinfo);
}

typedef struct s_idx_fn_ctx
{
	int				cur_min;
	int				last_min;
	size_t			idx;
	int				val;
	size_t			i;
}	t_idx_fn_ctx;

static int	index_items(t_stack_info *sinfo)
{
	t_idx_fn_ctx	ctx;

	ctx.last_min = INT_MIN;
	ctx.cur_min = INT_MAX;
	ctx.idx = 0;
	while (ctx.idx < stack_height(sinfo->stack))
	{
		ctx.i = 0;
		while (ctx.i < stack_height(sinfo->stack))
		{
			ctx.val = arr_int_get(stack_view(sinfo->stack).raw, ctx.i);
			if ((ctx.idx == 0 && ctx.val >= ctx.last_min
						&& ctx.val <= ctx.cur_min)
					|| (ctx.val > ctx.last_min && ctx.val <= ctx.cur_min))
			{
				ctx.cur_min = ctx.val;
			}
			ctx.i++;
		}
		stack_info_getitem_byvalue(sinfo, ctx.cur_min)->target_pos = ctx.idx;
		ctx.last_min = ctx.cur_min;
		ctx.cur_min = INT_MAX;
		ctx.idx++;
	}
	return (0);
}
// vi: noet sw=4 ts=4:
