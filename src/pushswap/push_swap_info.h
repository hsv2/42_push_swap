/* ************************************************************************** */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*   By: ablanken <ablanken at student dot 42barcelona dot com                */
/*                                                                            */
/*   Created: foo bar by ablanken                                             */
/*   Updated: foo bar by Andrea Blanke                                        */
/*                                                                            */
/* ************************************************************************** */
#ifndef  PUSH_SWAP_INFO_H
# define PUSH_SWAP_INFO_H

# include <stddef.h>

typedef struct s_stack	t_stack;

typedef struct s_stackitem_info
{
	int		val;
	size_t	pos;
	size_t	target_pos;
}	t_stackitem_info;

typedef struct s_stack_info
{
	t_stackitem_info	*items;
	int					max;
	int					min;
	const t_stack		*stack;
}	t_stack_info;

t_stack_info		*stack_info_new(const t_stack *s);
void				*stack_info_del(t_stack_info *sinfo);

t_stack_info		*stack_info_update(t_stack_info *sinfo, int reindex);

t_stackitem_info	*stack_info_getitem_bypos(const t_stack_info *sinfo,
						size_t pos);
t_stackitem_info	*stack_info_getitem_bytarget(const t_stack_info *sinfo,
						size_t pos);
t_stackitem_info	*stack_info_getitem_byvalue(const t_stack_info *sinfo,
						int value);

#endif
// vi: noet sw=4 ts=4:
