/* ************************************************************************** */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*   By: ablanken <ablanken at student dot 42barcelona dot com                */
/*                                                                            */
/*   Created: foo bar by ablanken                                             */
/*   Updated: foo bar by Andrea Blanke                                        */
/*                                                                            */
/* ************************************************************************** */
#ifndef  PUSH_SWAP_CTX_H
# define PUSH_SWAP_CTX_H

# include <push_swap.h>

# include "push_swap_info.h"

typedef struct s_push_swap_ctx
{
	t_stack			*stack_a;
	t_stack			*stack_b;
	t_stack_info	*sinfo;
	int				(*cb)(t_stack_view, t_stack_view, t_push_swap_op, void *);
	void			*cb_payload;
}					t_push_swap_ctx;

#endif
// vi: noet sw=4 ts=4:
