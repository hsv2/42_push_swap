/* ************************************************************************** */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*   By: ablanken <ablanken at student dot 42barcelona dot com                */
/*                                                                            */
/*   Created: foo bar by ablanken                                             */
/*   Updated: foo bar by Andrea Blanke                                        */
/*                                                                            */
/* ************************************************************************** */
#include "push_swap.h"
#include "push_swap_ctx.h"

int	push_swap_exec(t_push_swap_ctx *ctx, t_push_swap_op op)
{
	if (op == ps_op_pa)
		push_swap_push(ctx->stack_b, ctx->stack_a);
	else if (op == ps_op_pb)
		push_swap_push(ctx->stack_a, ctx->stack_b);
	else if (op == ps_op_ra)
		push_swap_rot(ctx->stack_a, NULL, 1);
	else if (op == ps_op_rb)
		push_swap_rot(ctx->stack_b, NULL, 1);
	else if (op == ps_op_rr)
		push_swap_rot(ctx->stack_a, ctx->stack_b, 1);
	else if (op == ps_op_rra)
		push_swap_rot(ctx->stack_a, NULL, -1);
	else if (op == ps_op_rrb)
		push_swap_rot(ctx->stack_b, NULL, -1);
	else if (op == ps_op_rrr)
		push_swap_rot(ctx->stack_a, ctx->stack_b, -1);
	else if (op == ps_op_sa)
		push_swap_swap(ctx->stack_a, NULL);
	else if (op == ps_op_sb)
		push_swap_swap(ctx->stack_b, NULL);
	else if (op == ps_op_ss)
		push_swap_swap(ctx->stack_a, ctx->stack_b);
	return (ctx->cb(stack_view(ctx->stack_a), stack_view(ctx->stack_b),
				op, ctx->cb_payload));
}
// vi: noet sw=4 ts=4:
