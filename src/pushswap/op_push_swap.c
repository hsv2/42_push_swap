/* ************************************************************************** */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*   By: ablanken <ablanken at student dot 42barcelona dot com                */
/*                                                                            */
/*   Created: foo bar by ablanken                                             */
/*   Updated: foo bar by Andrea Blanke                                        */
/*                                                                            */
/* ************************************************************************** */

#include <push_swap.h>

#include "push_swap_ctx.h"

int	push_swap_push(t_stack *src, t_stack *dst)
{
	if (src && dst)
	{
		if (stack_push(dst, stack_pop(src).item).size)
			return (0);
	}
	return (-1);
}

int	push_swap_swap(t_stack *s1, t_stack *s2)
{
	if (s1)
		if (!stack_swap(s1))
			return (-1);
	if (s2)
		if (!stack_swap(s2))
			return (-1);
	return (0);
}

int	push_swap_rot(t_stack *s1, t_stack *s2, int dir)
{
	t_stack	*(*rot)(t_stack *, unsigned int);

	rot = stack_lrotate;
	if (dir < 0)
		rot = stack_rrotate;
	if (s1)
		if (!rot(s1, 1))
			return (-1);
	if (s2)
		if (!rot(s2, 1))
			return (-1);
	return (0);
}
// vi: noet sw=4 ts=4:
