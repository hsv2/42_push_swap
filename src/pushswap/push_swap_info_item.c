/* ************************************************************************** */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*   By: ablanken <ablanken at student dot 42barcelona dot com                */
/*                                                                            */
/*   Created: foo bar by ablanken                                             */
/*   Updated: foo bar by Andrea Blanke                                        */
/*                                                                            */
/* ************************************************************************** */
#include "push_swap_info.h"

#include <ft_stack.h>

t_stackitem_info	*stack_info_getitem_bypos(const t_stack_info *sinfo,
	size_t pos)
{
	size_t	i;

	i = 0;
	while (i < stack_maxheight(sinfo->stack))
	{
		if (sinfo->items[i].pos == pos)
			return (&sinfo->items[i]);
		i++;
	}
	return (NULL);
}

t_stackitem_info	*stack_info_getitem_bytarget(const t_stack_info *sinfo,
	size_t pos)
{
	size_t	i;

	i = 0;
	while (i < stack_maxheight(sinfo->stack))
	{
		if (sinfo->items[i].target_pos == pos)
			return (&sinfo->items[i]);
		i++;
	}
	return (NULL);
}

t_stackitem_info	*stack_info_getitem_byvalue(const t_stack_info *sinfo,
	int val)
{
	size_t	i;

	i = 0;
	while (i < stack_maxheight(sinfo->stack))
	{
		if (sinfo->items[i].val == val)
			return (&sinfo->items[i]);
		i++;
	}
	return (NULL);
}
// vi: noet sw=4 ts=4:
