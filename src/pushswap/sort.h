/* ************************************************************************** */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*   By: ablanken <ablanken at student dot 42barcelona dot com                */
/*                                                                            */
/*   Created: foo bar by ablanken                                             */
/*   Updated: foo bar by Andrea Blanke                                        */
/*                                                                            */
/* ************************************************************************** */
#ifndef  SORT_H
# define SORT_H

# include <stddef.h>

typedef struct s_push_swap_ctx	t_push_swap_ctx;
typedef struct s_stack			t_stack;

void	range_presort(t_push_swap_ctx *ctx, size_t range);
void	move_item(t_push_swap_ctx *ctx, t_stack *s, size_t sidx, size_t didx);

#endif
// vi: noet sw=4 ts=4:
