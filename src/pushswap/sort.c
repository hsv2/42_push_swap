/* ************************************************************************** */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*   By: ablanken <ablanken at student dot 42barcelona dot com                */
/*                                                                            */
/*   Created: foo bar by ablanken                                             */
/*   Updated: foo bar by Andrea Blanke                                        */
/*                                                                            */
/* ************************************************************************** */
#include "push_swap_ctx.h"

#include <limits.h>
#include <stdlib.h>

#include "sort.h"

int	push_swap_sort(t_push_swap_ctx *ctx)
{
	ctx->sinfo = stack_info_new(ctx->stack_a);
	if (!ctx->sinfo)
		return (-1);
	range_presort(ctx, stack_height(ctx->stack_a) / 10);
	ctx->sinfo = stack_info_del(ctx->sinfo);
	return (0);
}


// vi: noet sw=4 ts=4:
