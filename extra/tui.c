#include "tui.h"
#include "libft.h"
#include "push_swap.h"

void plot_graph(WINDOW *win, t_stack_view s, int max, int min)
{
    const int offset = 2;

    if (!s.raw)
        return;

    werase(win);
    box(win, 0, 0);

    int h, w;
    getmaxyx(win, h, w);
    w -= 2 * offset;
    h -= 2 * offset;

    for (int x = 0; x <= w; x++)
    {
        const size_t item_n = lerp(0, s.maxheight, linear(0, w, x));
        const int value = arr_int_get(s.raw, item_n);
        const int bar_h = lerp(0, h, linear(min, max, value));
        for (int y = 0; item_n < s.top && y <= h && bar_h >= y; y++)
        {
            mvwaddch(win, h - y + offset, x + offset, ACS_BLOCK);
        }
    }
}

void print_op(WINDOW *win, t_push_swap_op op)
{
    int h, w;
    getmaxyx(win, h, w);

    werase(win);
    box(win, 0, 0);
    const char *const opname = push_swap_name_from_op(op);
    mvwaddstr(win, h / 2, (w - ft_strlen(opname)) / 2, opname);
}
