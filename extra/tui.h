#ifndef TUI_H
#define TUI_H

#include <curses.h>

#include <push_swap.h>

static inline float lerp(float a, float b, float t)
{
    return a + t * (b - a);
}

static inline float linear(float a, float b, float t)
{
    return (t - a) / (b - a);
}

void plot_graph(WINDOW *w, t_stack_view s, int max, int min);
void print_op(WINDOW *w, t_push_swap_op);

#endif
