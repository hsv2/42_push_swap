#include <stdio.h>

#include "../src/pushswap/push_swap_ctx.h"
#include "tui.h"

typedef struct {
    WINDOW *wstack_a;
    WINDOW *wstack_b;
    WINDOW *wop;
    int    stack_max;
    int    stack_min;
} t_cb_payload;

static void print_stack(t_stack *s)
{
    const t_stack_view	view = stack_view(s);

    for (size_t i = 0; i < view.top; i++)
        printf("%d  ", arr_int_get(view.raw, i));
    putchar('\n');
}

static int push_swap_cb(t_stack_view a, t_stack_view b, t_push_swap_op op, void *payload)
{
    t_cb_payload* p = payload;

    plot_graph(p->wstack_a, a, p->stack_max, p->stack_min);
    plot_graph(p->wstack_b, b, p->stack_max, p->stack_min);
    print_op(p->wop, op);
    wrefresh(p->wop);
    wrefresh(p->wstack_a);
    wrefresh(p->wstack_b);
    getch();
    return 1;
}

int main(int c, char **v)
{
    if (c < 2)
        return -1;
    t_stack *stack_a = push_swap_stack_from_args(&v[1], c - 1);
    if (!stack_a)
        return -2;
    t_push_swap_ctx *ctx = push_swap_ctx_new(stack_a);
    if (!ctx)
        return -3;

    WINDOW* wstack_a;
    WINDOW* wstack_b;
    WINDOW* wopname;

    initscr();
    cbreak();
    noecho();
//    refresh();
    wstack_a = newwin(LINES - 3, COLS / 2 - 1, 0, 0);
    wstack_b = newwin(LINES - 3, COLS / 2 - 1, 0, COLS / 2);
    wopname = newwin(3, COLS, LINES - 3, 0);

    t_cb_payload payload = {
        wstack_a,
        wstack_b,
        wopname,
        0,
        0,
    };
    stack_int_maxmin(ctx->stack_a, &payload.stack_max, &payload.stack_min);
    push_swap_step_cb(ctx, push_swap_cb, &payload);

    push_swap_cb(stack_view(stack_a), stack_view(ctx->stack_b), ps_op_undefined, &payload);

    push_swap_sort(ctx);
    getch();

    push_swap_ctx_del(ctx);
    delwin(wstack_a);
    delwin(wstack_b);
    delwin(wopname);
    endwin();
    print_stack(stack_a);
    stack_del(stack_a);
    return 0;
}
