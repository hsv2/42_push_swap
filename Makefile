NAME				= push_swap
CFLAGS				= -Ofast \
					  -march=native \
					  -pipe \
					  -Wall \
					  -Wextra \
					  -Wpedantic
CPPFLAGS			= -I$(LFTDIR) \
					  -Iinclude \
					  -MMD \
					  -MP
LDFLAGS				= -L$(LFTDIR) \
					  -L$(DIST_DIR)
LDLIBS				= -lftstack \
					  -lftarray \
					  -lft
SANITIZE			= -fsanitize=address,undefined
DEBUG				= -O0 \
					  -g3 \
					  -ggdb
DIST_DIR			= dist/
LFTDIR				= libft/
LFT					= $(addprefix $(LFTDIR), libft.a)

ARRAY_SRC			= $(addprefix src/array/, \
					  ft_array_life.c \
					  func.c \
					  func_extra.c \
					  )
LARRAY				= $(addprefix $(DIST_DIR), libftarray.a)
STACK_SRC			= $(addprefix src/stack/, \
					  attr.c \
					  op_common.c \
					  op_extra.c \
					  stack_int.c \
					  stack_life.c \
					  )
LSTACK				= $(addprefix $(DIST_DIR), libftstack.a)
PUSHWAP_SRC			= $(addprefix src/pushswap/, \
					  exec.c \
					  move.c \
					  op.c \
					  op_push_swap.c \
					  presort.c \
					  push_swap_ctx.c \
					  push_swap_info.c \
					  push_swap_info_item.c \
					  push_swap_info_update.c \
					  sort.c \
					  )
LPUSHSWAP			= $(addprefix $(DIST_DIR), libpushswap.a)

TEST_STACK_SRC		= test/stack.c
TEST_STACK			= $(addprefix $(DIST_DIR), test_stack)

TEST_PUSHSWAP_SRC	= test/push_swap.c
TEST_PUSHSWAP		= $(addprefix $(DIST_DIR), test_push_swap)

TUI_MONITOR_SRC		= extra/main.c \
					  extra/tui.c
TUI_MONITOR			= $(addprefix $(DIST_DIR), pushdemon)

ALL_TEST			= $(TEST_STACK) $(TEST_PUSHSWAP)
ALL_SRC				= $(ARRAY_SRC) $(STACK_SRC) $(TEST_STACK_SRC) \
					  $(TUI_MONITOR_SRC) $(PUSHWAP_SRC) $(TEST_PUSHSWAP_SRC)
ALL_OBJ				= $(ALL_SRC:.c=.o)
DEPENDENCIES		= $(ALL_SRC:.c=.d)

all: $(TUI_MONITOR) test;

clean:
	$(MAKE) -C $(LFTDIR) clean
	$(RM) $(ALL_OBJ) $(DEPENDENCIES)

fclean: clean
	$(MAKE) -C $(LFTDIR) fclean
	$(RM) -r $(DIST_DIR)

debug: CFLAGS += $(DEBUG)
debug: | fclean all

test: $(ALL_TEST)

sanitize: CFLAGS += $(DEBUG) $(SANITIZE)
sanitize: | fclean all

$(LFT):
	$(MAKE) -C $(LFTDIR)

$(LARRAY): $(ARRAY_SRC:.c=.o)
	mkdir -p $(DIST_DIR)
	$(AR) -rs $@ $^

$(LSTACK): $(STACK_SRC:.c=.o)
	mkdir -p $(DIST_DIR)
	$(AR) -rs $@ $^

$(LPUSHSWAP): $(PUSHWAP_SRC:.c=.o)
	mkdir -p $(DIST_DIR)
	$(AR) -rs $@ $^

$(TEST_STACK): $(TEST_STACK_SRC:.c=.o) $(LSTACK) $(LARRAY) $(LFT)
	mkdir -p $(DIST_DIR)
	$(CC) -o $@ $(TEST_STACK_SRC:.c=.o) $(CFLAGS) $(LDFLAGS) $(LDLIBS)

$(TEST_PUSHSWAP): $(TEST_PUSHSWAP_SRC:.c=.o) $(LPUSHSWAP) $(LSTACK) $(LARRAY) $(LFT)
	mkdir -p $(DIST_DIR)
	$(CC) -o $@ $(TEST_PUSHSWAP_SRC:.c=.o) $(CFLAGS) $(LDFLAGS) -lpushswap $(LDLIBS)

$(TUI_MONITOR): $(TUI_MONITOR_SRC:.c=.o) $(LPUSHSWAP) $(LSTACK) $(LARRAY) $(LFT)
	mkdir -p $(DIST_DIR)
	$(CC) -o $@ $(TUI_MONITOR_SRC:.c=.o) $(CFLAGS) $(LDFLAGS) -lpushswap $(LDLIBS) $(shell pkg-config --libs ncurses)

.PHONY: all clean debug fclean test sanitize;
-include $(DEPENDENCIES)
