/* ************************************************************************** */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*   By: ablanken <ablanken at student dot 42barcelona dot com                */
/*                                                                            */
/*   Created: foo bar by ablanken                                             */
/*   Updated: foo bar by Andrea Blanke                                        */
/*                                                                            */
/* ************************************************************************** */

#ifndef  FT_ARRAY_ITEM_H
# define FT_ARRAY_ITEM_H

# include <stddef.h>

typedef struct s_array_item
{
	size_t	size;
	void	*item;
}			t_array_item;

#endif
// vi: noet sw=4 ts=4:
