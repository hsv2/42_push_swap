/* ************************************************************************** */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*   By: ablanken <ablanken at student dot 42barcelona dot com                */
/*                                                                            */
/*   Created: foo bar by ablanken                                             */
/*   Updated: foo bar by Andrea Blanke                                        */
/*                                                                            */
/* ************************************************************************** */

#ifndef  PUSH_SWAP_H
# define PUSH_SWAP_H

# include <ft_stack.h>

typedef struct s_push_swap_ctx	t_push_swap_ctx;

typedef enum e_push_swap_op
{
	ps_op_pa,
	ps_op_pb,
	ps_op_ra,
	ps_op_rb,
	ps_op_rr,
	ps_op_rra,
	ps_op_rrb,
	ps_op_rrr,
	ps_op_sa,
	ps_op_sb,
	ps_op_ss,
	ps_op_undefined,
}	t_push_swap_op;

t_push_swap_ctx	*push_swap_ctx_new(t_stack *stack_a);
void			*push_swap_ctx_del(t_push_swap_ctx *ctx);
t_stack			*push_swap_stack_from_args(char **args, int count);
void			push_swap_step_cb(t_push_swap_ctx *ctx,
					int (*cb)(t_stack_view, t_stack_view, t_push_swap_op,
						void *), void *cb_payload);

int				push_swap_sort(t_push_swap_ctx *ctx);

int				push_swap_exec(t_push_swap_ctx *ctx, t_push_swap_op op);
int				push_swap_push(t_stack *src, t_stack *dst);
int				push_swap_rot(t_stack *s1, t_stack *s2, int direction);
int				push_swap_swap(t_stack *s1, t_stack *s2);

const char		*push_swap_name_from_op(t_push_swap_op op);
t_push_swap_op	push_swap_op_from_name(const char *opname);

#endif

// vi: noet sw=4 ts=4:
