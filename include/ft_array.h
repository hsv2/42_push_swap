/* ************************************************************************** */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*   By: ablanken <ablanken at student dot 42barcelona dot com                */
/*                                                                            */
/*   Created: foo bar by ablanken                                             */
/*   Updated: foo bar by Andrea Blanke                                        */
/*                                                                            */
/* ************************************************************************** */

#ifndef  FT_ARRAY_H
# define FT_ARRAY_H

# include <stddef.h>

# include <ft_array_item.h>

# define ARRAY_NOITEM (t_array_item){0, NULL}

typedef struct s_array		t_array;

t_array			*arr_new(size_t itemsize, size_t itemcount);
void			*arr_del(t_array *a);

t_array_item	arr_get(const t_array *a, size_t idx);
t_array_item	arr_set(t_array *a, size_t idx, void *item);

t_array			*arr_rev(t_array *a, size_t start, size_t end);
t_array			*arr_swap(t_array *a, size_t idxa, size_t idxb);

size_t			arr_size(const t_array *a);
size_t			arr_itemsize(const t_array *a);

int				arr_int_get(const t_array *a, size_t idx);
int				arr_int_uniq(const t_array *a, size_t start, size_t end);

#endif
// vi: noet sw=4 ts=4:
