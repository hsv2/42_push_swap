/* ************************************************************************** */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/*   By: ablanken <ablanken at student dot 42barcelona dot com                */
/*                                                                            */
/*   Created: foo bar by ablanken                                             */
/*   Updated: foo bar by Andrea Blanke                                        */
/*                                                                            */
/* ************************************************************************** */
#ifndef  FT_STACK_H
# define FT_STACK_H

# include <stddef.h>

# include <ft_array.h>

typedef struct s_stack	t_stack;
typedef struct s_stack_view
{
	size_t			maxheight;
	size_t			top;
	const t_array	*raw;
}	t_stack_view;

// Allocate a stack of c elements of size s
t_stack			*stack_new(size_t count, size_t size);
// Free a stack and return NULL
void			*stack_del(t_stack *s);

// Get the top item of the stack or NULL on error
t_array_item	stack_peek(const t_stack *s);
// Pop from the top of the stack and return popped item or NULL on error
t_array_item	stack_pop(t_stack *s);
// Push to the top of the stack and return pushed item or NULL on error
t_array_item	stack_push(t_stack *s, void *item);

// Left rotate the stack
t_stack			*stack_lrotate(t_stack *s, unsigned int step);
// Right rotate the stack
t_stack			*stack_rrotate(t_stack *s, unsigned int step);
// Swap the two topmost items of the stack
t_stack			*stack_swap(t_stack *s);

size_t			stack_height(const t_stack *s);
size_t			stack_itemsize(const t_stack *s);
size_t			stack_maxheight(const t_stack *s);
t_stack_view	stack_view(const t_stack *s);

int				stack_int_maxmin(const t_stack *s, int *max, int *min);
int				stack_int_issorted(const t_stack *s);
int				stack_int_uniq(const t_stack *s);

#endif
// vi: noet sw=4 ts=4:
